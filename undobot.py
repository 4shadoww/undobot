#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" 
Copyright (C) 2018 Noa-Emil Nissinen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.    If not, see <https://www.gnu.org/licenses/>.
"""

"""Dependencies: 
python-mwapi: https://github.com/mediawiki-utilities/python-mwapi
sseclient: https://pypi.org/project/sseclient-py/
tinydb: https://pypi.org/project/tinydb/
"""

lang_dict = {
    "fi": "Botti kumosi automaattisesti mahdollisesti haitallisen muutoksen %s. [[%s|Ilmoita virheestä]].",
    "en": "A bot reverted automatically a possibly harmful revision %s. [[%s|Report false positive]]."
}

version = "1.0"

disclaimer ="""Undobot %s
Copyright (C) 2018 4shadoww
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law."""

# Import python modules
import logging
import os
import sys
import json
from urllib.request import urlopen
from urllib.parse import urlencode
import traceback
import argparse

# Import third-party modules
import mwapi
from sseclient import SSEClient as EventSource
from tinydb import TinyDB

# Import user config
import user_config

# mwapi session
session = None

# Get main path
class path:
    def main():
        """Get main path
        Returns:
            String which contains the main module's path
        """
        return os.path.dirname(os.path.abspath(sys.modules['__main__'].__file__)) + "/"

# Config loader
class config_loader:
    config = {
        "lang": "en",
        "site": "https://en.wikipedia.org",
        "api_path": "/w/api.php",
        "ores_api": "https://ores.wikimedia.org/v3/scores/enwiki",
        "stream_url": "https://stream.wikimedia.org/v2/stream/recentchange",
        "ores_config": {
            "wiki": "enwiki",
            "models": [
                "goodfaith",
                "damaging"
            ],
            "scores": {
                "goodfaith": {
                    "false_min": 0.85,
                    "true_max": 0.25
                },
                "damaging": {
                    "true_min": 0.85,
                    "false_max": 0.25
                }
            }
        },
        "test": True,
        "namespaces": [0],
        "enable_database": True,
        "save_to_database": "any",
        "database": "data.json",
        "enable_log": False,
        "ignore_bots": True,
        "ignored_groups": ["autoconfirmed"],
        "false_positives_page": "User:TheBot/FalsePositives"
    }
    def merge_config(to_config, from_config):
        """Merge loaded config with default config"""
        for key in from_config:
            to_config[key] = from_config[key]
            
        return to_config

    def load_config(filename=path.main() + "config.json"):
        """Load config from file

        Returns:
            True if config was loaded

            False if config was not loaded and default config file was created
        """

        global config
        # If config file doesn't exist create one and exit
        if not os.path.isfile(filename):
            with open(filename, "w") as config_f:
                json.dump(config_loader.config, config_f, indent=4, separators=(',', ': '))

            return False
        # Load config file and merge it with default config
        with open(filename, "r") as config_f:
            temp_config = json.load(config_f)

        config_loader.merge_config(config_loader.config, temp_config)

        return True

# Api
class api:
    def parameter_maker(values):
        """Make parameter from list
       
        Returns:
            String if values is for example: ['foo', 'bar'] -> 'bar|foo'
        """
        if type(values) != list:
            return values

        final_str = ""
        for i in range(len(values)):
            final_str += str(values[i])

            if i < len(values) - 1:
                final_str += "|"

        return final_str

    def get_score(revids, models=["goodfaith", "damaging"]):
        """Get ores scores for the revid

        Returns:
            Ores api response
        """
        params = {
            "revids": api.parameter_maker(revids),
            "models": api.parameter_maker(models)
        }

        request_url = config_loader.config["ores_api"] + "?"  + urlencode(params)

        request = urlopen(request_url)
        try:
            request = request.read().decode('utf8')
            return json.loads(request)
        except AttributeError:
            return False
        
    def get_token(token_types):
        """Get tokens from wikipedia

        Returns:
            Requested tokens in a dict"""
        params = {
            "action": "query",
            "meta": "tokens",
            "type": api.parameter_maker(token_types)
        }
        return session.get(params)["query"]["tokens"]

    def undo_revision(title, revision, summary):
        """Undo the revision"""
        params = {
            "action": "edit",
            "title": title,
            "undo": revision,
            "summary": summary,
            "token": api.get_token("csrf")["csrftoken"]
        }
        session.post(params)
 
    def get_user_groups(user):
        """Get user's groups
        Returns list of groups

        Returns False if response doesn't include list of groups
        """
        params = {
            "action": "query",
            "list": "users",
            "ususers": user,
            "usprop": "groups"
        }
        query = session.get(params)

        if "groups" in query["query"]["users"][0]:
            return query["query"]["users"][0]["groups"]

        return False

def setup_argparser():
    parser.add_argument("-v", "--version", help="show undobot's version", required=False, action="store_true")
    parser.add_argument("-t", "--test", help="don't save changes", required=False, action="store_true")
    parser.add_argument("-cf", "--config-file", help="set configuration file's path", required=False)
    
class LessThanFilter(logging.Filter):
    def __init__(self, exclusive_maximum, name=""):
        super(LessThanFilter, self).__init__(name)
        self.max_level = exclusive_maximum

    def filter(self, record):
        return 1 if record.levelno < self.max_level else 0
    
def setup_logging():
    """Set up and configure logger using config_loader.config"""
    global logger
    # Logging
    logger = logging.getLogger("infolog")
    logger.setLevel(logging.DEBUG)
    # Formatter
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    # Stream
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    ch.addFilter(LessThanFilter(logging.ERROR))
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    # Error stream
    eh = logging.StreamHandler(sys.stderr)
    eh.setLevel(logging.ERROR)
    eh.setFormatter(formatter)
    logger.addHandler(eh)
    # Info log
    if config_loader.config["enable_log"]:
        il = logging.FileHandler(config_loader.config["log_path"])
        il.setLevel(logging.DEBUG)
        il.setFormatter(formatter)
        logger.addHandler(il)

    
        
def login():
    """Login to wikipedia with mwapi"""
    global session
    session = mwapi.Session(config_loader.config["site"], user_agent="Undobot/1.0", api_path=config_loader.config["api_path"])
    session.login(user_config.username, user_config.password)

def bot_filter(is_bot):
    """Filters bots
    Returns:
        True if bot filtering is not enabled or user is not a bot

        False if bot filter enabled and user is a bot
    """
    if config_loader.config["ignore_bots"] and is_bot:
        return False

    return True

def user_group_filter(user_name):
    """Filter users
    Returns:
        True if user isn't in filttered group(s)

        False if users is in filttered group(s)
    """
    groups = api.get_user_groups(user_name)
    
    # If no groups at all return False
    if not groups:
        return False
    
    for group in groups:
        if group in config_loader.config["ignored_groups"]:
            return False

    return True

def must_revert(scores, revid):
    """Checks ores scores and decides should revision be reverted
    Returns:
        True if should be reverted

        False if should not be reverted
    """
    for model in config_loader.config["ores_config"]["models"]:
        ores_model = config_loader.config["ores_config"]["scores"][model]
        score = scores[config_loader.config["ores_config"]["wiki"]]["scores"][str(revid)][model]["score"]["probability"]

        if "true_min" in ores_model and "false_max" in ores_model:
            if ores_model["true_min"] > score["true"] or ores_model["false_max"] < score["false"]:
                return False
            
        if "true_max" in ores_model and "false_min" in ores_model:
            if ores_model["true_max"] < score["true"] or ores_model["false_min"] > score["false"]:
                return False
            
    return True

def comment_maker(revid):
    """Generates summary for undo
    Returns:
        String which contains the comment on language you chose
    """
    return lang_dict[config_loader["lang"]] % (str(revid), config_loader.config["false_positives_page"])

def start_event_stream():
    # Start event stream
    wiki = config_loader.config["lang"] + "wiki"
    for event in EventSource(config_loader.config["stream_url"]):
        # Filter everything other than message events
        if event.event == "message":
            # Parse the event
            try:
                change = json.loads(event.data)
            except ValueError:
                continue
            # Filter events
            # Apply bot filtering and user group filtering
            if (change["wiki"] == wiki and change["type"] == "edit" and change["namespace"] in config_loader.config["namespaces"]
            and bot_filter(change["bot"]) and user_group_filter(change["user"])):
                
                logger.info("checking \"%s\" revision \"%s\" by \"%s\"" % (change["title"], change["revision"]["new"], change["user"]))
                # Get scores from ores
                scores = api.get_score(change["revision"]["new"])
                # Check should revision be reverted
                if must_revert(scores, change["revision"]["new"]):
                    # Revert if no test mode enabled
                    if config_loader.config["test"] == False:
                        api.undo_revision(change["title"], change["revision"]["new"], comment_maker(change["revision"]["new"]))

                    # Log to database if enabled
                    if config_loader.config["enable_database"] and config_loader.config["save_to_database"] == "positive":
                        db.insert({"title": change["title"], "revid": change["revision"]["new"], "user": change["user"], "timestamp": change["timestamp"], "scores": scores, "reverted": True})

                 # Log to database if enabled
                elif config_loader.config["enable_database"] and (config_loader.config["save_to_database"] == "negative" or config_loader.config["save_to_database"] == "any"):
                    db.insert({"title": change["title"], "revid": change["revision"]["new"], "user": change["user"], "timestamp": change["timestamp"], "scores": scores, "reverted": False})

def main():
    global db
    global parser
    
    # Set up arg parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
    setup_argparser()
    args = parser.parse_args()
    
    if "help" in args:
      parser.print_help()
      os._exit(0)

    if "version" in args and args.version:
        print("Undobot %s" % version)
        os._exit(0)
      
    try:
        # Print disclaimer
        print((disclaimer + "\n") % version)
        
        # Set config file path
        config_path = "config.json"
        if args.config_file:
            config_path = args.config_file
        
        # Load config
        # If new config created then exit
        if not config_loader.load_config(filename=config_path):
            print("default config created to %s" % config_path)
            print("please configure bot before running it again")
            os._exit(0)

        # Overwrite test with True if it's defined in args
        if args.test:
            config_loader.config["test"] = True
            
        # Set up logging
        setup_logging()
 
         # Check is selected language available
        if not config_loader.config["lang"] in lang_dict:
            logger.critical("language \"%s\" is not availabe on lang_dict\ncannot continue halting..." % config_loader.config["lang"])
        
        # Print message if test mode is enabled
        if config_loader.config["test"]:
            logger.info("test mode is enabled and no changes will be sent to the server")

        # Open database if enabled
        if config_loader.config["enable_database"]:
            db = TinyDB(config_loader.config["database"])
            
        logger.info("logging in")
        # Login
        login()
        logger.info("logged in as %s" % user_config.username)
        
        # Start event stream
        start_event_stream()
                        
    # Interrupt handling
    except KeyboardInterrupt:
        print("keyboardinterrupt received exiting...")
    except:
        # If logger isn't properly set up then just print the error
        try:
            logger.critical("unexcepted interrupt raised:")
            logger.critical(traceback.format_exc())
        except:
            print("unexcepted interrupt raised:")
            print(traceback.format_exc())
            
        # Return error to shell
        print("halting...")
        sys.exit(1)

if __name__ == "__main__":
    main()
