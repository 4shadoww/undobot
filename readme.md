Undobot
=======

Undobot is an anti-vandalism bot for wikimedia projects which reverts possibly harmful edits. It uses a service called ORES to detect harmful edits.

Configuration
=============

Username and password should be stored in `user_config.py` file in variables `username` and `password`.

When setting up undobot you should first run bot once `./undobot.py` to get default configuration file. Next you should edit the generated configuration file to suit your needs. Look down below for explanations of variables of the configuration file.

Configuration file
------------------

| Key                | Example value                                       | Description                                |
|:-------------------|:----------------------------------------------------|:-------------------------------------------|
|lang                |"en"                                                 |Language of edit's summary                  |
|site                |"https://en.wikipedia.org"                           |Url of the mediawiki                        |
|api_path            |"/w/api.php"                                         |API path                                    |
|ores_api            |"https://ores.wikimedia.org/v3/scores/enwiki"        |ORES api path                               |
|stream_url          |"https://stream.wikimedia.org/v2/stream/recentchange"|Recent changes stream url                   |
|ores_config         |                                                     |ORES model config                           |
|test                |true                                                 |Declares is test mode enabled               |
|namespaces          |[0]                                                  |List of namespaces which will be reviewed   |
|enable_database     |true                                                 |Save results of review to database          |
|save_to_database    |"any"                                                |Save positive, negative or any result       |
|enable_log          |true                                                 |Enable logger                               |
|ignore_bots         |true                                                 |Don't review bots' edits                    |
|ignored_groups      |["autoconfirmed"]                                    |Ignore users who belong to these groups     |
|false_positives_page|User:TheBot/FalsePositives                           |Wikipage which will be linked in the summary|

### ores_config

| Key  | Example value                                    | Description     |
|:-----|:-------------------------------------------------|:----------------|
|wiki  |"enwiki"                                          |Target wiki      |
|models|["goodfaith", "damaging"]                         |Models to be used|
|scores|"goodfaith": {"false_min": 0.85, "true_max": 0.25}|ORES score rules |


Usage
=====

usage: undobot.py [-h] [--test] [--config-file CONFIG_FILE]

optional arguments:  
  -h, --help                      show help message and exit  
  -v, --version                   show undobot's version  
  -t, --test                      don't save changes  
  -cf, --config-file CONFIG_FILE  set configuration file's path  

Dependencies
============
* python-mwapi: https://github.com/mediawiki-utilities/python-mwapi
* sseclient: https://pypi.org/project/sseclient-py/
* tinydb: https://pypi.org/project/tinydb/

Links
=====

* https://gitlab.com/4shadoww/undobot repository

License
=======
This software is distributed under the GPLv3 license.